var Config = {};

Config.auth = {};
Config.auth.jwtSecret = "ilovewineandseba";

Config.errorMsg = {};
Config.errorMsg.serverError = {"status": 500, "message": "Internal server error"};
Config.errorMsg.notFoundError = {"status": 404, "message": "Not found"};
Config.errorMsg.forbiddenError = {"status": 403, "message": "Forbidden. You do not own this document."};
Config.errorMsg.noLinkedAccError = {"status": 404, "message": "Link yourself to a maker first."};
Config.errorMsg.noMakerError = {"status": 500, "message": "The wine you are about to edit has no maker."};
Config.errorMsg.validationError = {"status": 400, "message": "Request validation failed."};

module.exports = Config;