var Config = require('./../Config/Config.js');
var Wine = require('./../models/wine');
var Recommendation = require('./../models/recommendation');

// Outlet for POST /api/wines/:wine_id/recommendations
exports.post = function(req, res) {
    if (Array.isArray(req.body)) {
        res.status(500).send(Config.errorMsg.serverError);
        return;
    }
    Wine.findById(
        req.params.wine_id,
        function(err, wine) {
            if (err) {
                res.status(500).send(Config.errorMsg.serverError);
            } else if (!wine) {
                res.status(404).send(Config.errorMsg.notFoundError);
            } else {
                console.log("Found wine " + wine);

                var recommendation = new Recommendation(req.body);
                wine.recommendations[wine.recommendations.length] = recommendation;
                wine.markModified('recommendations');

                console.log("Altered wine " + wine);

                wine.save(
                    {runValidators: true}, function(err) {
                    if (err) {
                        console.error(err);
                        if (err.name == 'ValidationError') {
                            res.status(400).json(err);
                        } else {
                            res.status(500).send(Config.errorMsg.serverError);
                        }
                    } else
                        res.status(201).json(wine);
                });
            }
    });
};

// Outlet for GET /api/wines/:wine_id/recommendations
exports.get = function(req, res) {
    Wine.findById(req.params.wine_id,function(err, wine) {
            if (err) {
                console.error(err);
                res.status(500).send(Config.errorMsg.serverError);
            } else if (!wine) {
                res.status(404).send(Config.errorMsg.notFoundError);
            } else {
                res.json(wine.recommendations);
            }
        });
};

// Outlet for DELETE /api/wines/:wine_id/recommendations/:recommendation_id
exports.delete = function(req, res) {
    //Only admins can delete recommendations
    if (req.user.isAdmin) {
        Wine.findByIdAndUpdate(
            req.params.wine_id,
            {$pull: {recommendations : { _id: req.params.recommendation_id}}},
            {
                safe: true
            },
            function(err, wine) {
                if (err) {
                    console.error(err);
                    res.status(500).send(Config.errorMsg.serverError);
                } else if (!wine) {
                    res.status(404).send(Config.errorMsg.notFoundError);
                } else {
                    res.status(201).json(wine._id);
                }
            }
        );
    } else {
        res.status(401).send(Config.errorMsg.forbiddenError);
    }
};