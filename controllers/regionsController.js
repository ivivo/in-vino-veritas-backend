var Config = require('./../Config/Config.js');
var Maker = require('./../models/maker');

// Outlet for GET /regions
exports.getRegions = function(req, res) {
    Maker.aggregate(
        { $project: { region: "$region"}},
        { $unwind: "$region"},
        { $group: { _id : { name: "$region.name"}}},
        { $sort: { "_id.type": 1, "_id.name": 1}},
        function (err, tags) {
            if (err) {
                console.error(err);
                res.status(500).send(Config.errorMsg.serverError);
            } else
                res.json(tags);
        });
};