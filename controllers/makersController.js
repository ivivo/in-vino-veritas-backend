var Config = require('./../Config/Config.js');
var Maker = require('./../models/maker');

// Outlet for POST /api/makers/
exports.post = function(req, res) {
    if (Array.isArray(req.body)) {
        res.status(500).send(Config.errorMsg.serverError);
        return;
    }

    var maker = new Maker(req.body);
    maker.user_id = req.user._id;

    maker.save(function(err) {
        if (err) {
            console.error(err);
            if (err.name == 'ValidationError'){
                res.status(400).json(err); //400 = bad request
            } else {
                res.status(500).send(Config.errorMsg.serverError);
            }
        } else
            res.status(201).json({message: 'Profil wurde erstellt',maker_id: maker._id});
    });
};

// Outlet for GET /api/makers
exports.get = function(req, res) {
    if (req.user.isAdmin) {
        Maker.find(function (err, makers) {
            if (err) {
                console.error(err);
                res.status(500).send(Config.errorMsg.serverError);
            } else
                res.json(makers);
        });
    }
    else {
        Maker.findOne({user_id: req.user._id}, function (err, maker) {
            if (err) {
                console.error(err);
                res.status(500).send(Config.errorMsg.serverError);
            } else if (!maker) {
                res.status(404).send(Config.errorMsg.noLinkedAccError);
            } else {
                res.json(maker);
            }
        });
    }
};

// Outlet for GET /api/makers/:maker_id
exports.getById = function (req, res) {
    Maker.findById(req.params.maker_id, function (err, maker) {
        if (err) {
            console.error(err);
            res.status(500).send(Config.errorMsg.serverError);
        } else if (!maker) {
            res.status(404).send(Config.errorMsg.notFoundError);
        } else {
            res.json(maker);
        }
    });
};

// Outlet for PUT /api/makers/:maker_id
exports.put = function (req, res) {
    //Authenticate Maker
    Maker.findById(req.params.maker_id, function (err, maker) {
        console.log("id of user: " + req.user._id + "\nid of item to be changed: " + maker.user_id);
        if ((String(req.user._id) == String(maker.user_id)) || (req.user.isAdmin)) {

            Maker.findByIdAndUpdate(
                req.params.maker_id,
                req.body,
                {
                    //pass the new object to cb function
                    new: true,
                    //run validations
                    runValidators: true
                }, function (err, maker) {
                    if (err) {
                        console.error(err);
                        if (err.name = 'ValidationError'){
                            res.status(400).json(err); //400 = bad request
                        } else {
                            res.status(500).send(Config.errorMsg.serverError);
                        }
                    } else {
                        res.json({message: 'Profiländerungen wurden gespeichert',maker: maker});
                    }
                });
        } else {
            res.status(401).send(Config.errorMsg.forbiddenError);
        }
    });

};

// Outlet for DELETE /api/makers/:maker_id
exports.delete = function(req, res) {
    //Only admins can delete makers
    if (req.user.isAdmin) {
        Maker.remove(
            {_id: req.params.maker_id},
            function(err, maker) {
                if (err) {
                    console.error(err);
                    res.status(500).send(Config.errorMsg.serverError);
                } else {
                    res.json({message: 'Winzer wurde erfolgreich gelöscht'});
                }
            }
        );
    } else {
        res.status(401).send(Config.errorMsg.forbiddenError);
    }
};