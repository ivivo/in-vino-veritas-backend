var Config = require('./../Config/Config.js');
var Wine = require('./../models/wine');
var Maker = require('./../models/maker');

var url = require('url');

// Outlet for POST /api/wines
exports.post = function(req, res) {
    //Do no accept Arrays
    if (Array.isArray(req.body)) {
        res.status(500).send(Config.errorMsg.serverError);
        return;
    }

    var wine = new Wine(req.body);

    //Create wine as currently logged in user
    if (req.user.isAdmin) {
        saveWine(wine, res);
    } else {
        Maker.findOne( {"user_id": req.user._id}, function(err, maker) {
            if (err) {
                console.error(err);
                res.status(500).send(Config.errorMsg.serverError);
            } else if (!maker) {
                res.status(404).send(Config.errorMsg.noLinkedAccError);
            } else {
                wine.maker = maker._id;
                saveWine(wine, res);
            }
        });
    }
};

function saveWine(wine, res) {
    wine.save(function(err) {
        if (err) {
            console.error(err);
            if (err.name == 'ValidationError'){
                res.status(400).json(err); //400 = bad request
            } else {
                res.status(500).send(Config.errorMsg.serverError);
            }
        } else
            res.status(201).json({message: "Wein gespeichert"});
    });
}

// Outlet for GET /api/wines
exports.get = function(req, res) {
    Wine.find()
        .populate('maker')
        .exec(function(err, wines) {
            if (err) {
                console.error(err);
                res.status(500).send(Config.errorMsg.serverError);
            } else
                res.json(wines);
        });
};

// Outlet for GET /api/wines/:wine_id
exports.getById = function(req, res) {
    Wine.findById(req.params.wine_id)
        .populate('maker')
        .exec(function(err, wine) {
        if (err) {
            console.error(err);
            res.status(500).send(Config.errorMsg.serverError);
        } else if (!wine) {
            res.status(404).send(Config.errorMsg.notFoundError);
        } else {
            res.json(wine);
        }
    });
};

// Outlet for GET /search/tags/:tag_names
// Returns all wines matching the tag_names
exports.getByTag = function(req, res) {
    var tag_names = req.params.tag_names.split(',');
    var query = Wine.find();
    for (var i = 0; i < tag_names.length; i++) {
        query = query.find({"tags.name": tag_names[i]});
    }
    query.populate('maker')
        .exec(function(err, wines) {
            if (err) {
                console.error(err);
                res.status(500).send(Config.errorMsg.serverError);
            } else if (!wines) {
                res.status(404).send(Config.errorMsg.notFoundError);
            } else {
                res.json(wines);
            }
        });
};

// Outlet for GET /search/filter/{filter_options}
// Search engine for discover page
exports.getByFilterOptions = function(req, res) {
    var query = url.parse(req.url).query
    var filters

    if (query) {
        // get filter options
        filters = JSON.parse(decodeURIComponent(query));

        var query = Wine.find();

        var type;

        // filter by type
        if (typeof type != 'undefined') {
            query = query.find({"tags.name": type});
        }

        // filter by tags
        var queryTags = [];
        for (var i in filters.tags) {
            var tag = filters.tags[i];
            queryTags.push({
                "tags.name" : tag.name
            })
            query = query.find({$and: queryTags});
        }

        // filter by range of year and price
        query = query.find({"year": {$gte : filters.year.min, $lte : filters.year.max}});
        query = query.find({"price": {$gte : filters.price.min, $lte : filters.price.max}});

        query.populate('maker');

        query
            .exec(function(err, wines) {
                if (err) {
                    console.error(err);
                    res.status(500).send(Config.errorMsg.serverError);
                } else if (!wines) {
                    res.status(404).send(Config.errorMsg.notFoundError);
                } else {

                    // filter by region
                    if (filters.region != null) {
                        wines = wines.filter(function(wine){
                            return wine.maker.region.name == filters.region;
                        })
                    }

                    res.json(wines);
                }
            });

        return;
    }

    var error = new Error('missing query');
    error.status = 400;
    console.log(error);
    return;
};

// Outlet for GET /api/makers/:maker_id/wines
exports.getByMaker = function(req, res) {
    var maker_id = req.params.maker_id;

    Wine.find({maker: maker_id})
        .populate('maker')
        .exec(function(err, wines) {
            if (err) {
                console.error(err);
                res.status(500).send(Config.errorMsg.serverError);
            } else if (!wines) {
                res.status(404).send(Config.errorMsg.notFoundError);
            } else {
                res.json(wines);
            }
        });
};

// Outlet for GET /api/own/wines
// Returns all wines owned by logged in maker
exports.getOwn = function(req, res) {
    //get logged in maker
    console.log("user is " + req.user._id);
    Maker.findOne({user_id: req.user._id}, function(err, maker) {
        if (err) {
            console.error(err);
            res.status(500).send(Config.errorMsg.serverError);
        } else if (!maker) {
            res.status(404).send(Config.errorMsg.noLinkedAccError);
        } else {

            //get wines
            Wine.find({maker: maker._id})
                .populate('maker')
                .exec(function(err, wines) {
                    if (err) {
                        console.error(err);
                        res.status(500).send(Config.errorMsg.serverError);
                    } else if (!wines) {
                        res.status(404).send(Config.errorMsg.notFoundError);
                    } else {
                        res.json(wines);
                    }
                });
        }
    });
};

// Outlet for PUT /wines/:wine_id
exports.put = function(req, res) { //TODO: DEBUG put function
    authenticateUser(req, res, updateWine);
};

function updateWine(req, res) {
    console.log("updating..");
    Wine.findByIdAndUpdate(
        req.params.wine_id,
        req.body,
        {
            //pass the new object to cb function
            new: true,
            //run validations
            runValidators: true
        }, function (err, wine) {
            if (err) {
                console.error(err);
                if (err.name == 'ValidationError'){
                    res.status(400).json(err); //400 = bad request
                } else {
                    res.status(500).send(Config.errorMsg.serverError);
                }
            } else {
                res.json({message: "Änderungen wurden gespeichert",wine: wine});
            }
        });
}

// Outlet for DELETE /wines/:wine_id
exports.delete = function(req, res) { //TODO: Debug delete function
    authenticateUser(req, res, removeWine);
};

function removeWine (req, res) {
    console.log("removing...");
    Wine.remove({
        _id: req.params.wine_id
    }, function(err, wine) {
        if (err) {
            console.error(err);
            res.status(500).send(Config.errorMsg.serverError);
        } else {
            res.json({message: 'Wein wurde erfolgreich gelöscht'});
        }
    });
}

function authenticateUser(req, res, callback) {
    //Allow only changing of own wines
    if (req.user.isAdmin) {
        callback(req, res);
    } else {
        Wine.findById(req.params.wine_id, function(err, wine) {
            if (wine) {
                //find maker of wine
                Maker.findById(wine.maker, function(err, maker) {
                    if (maker) {
                        if (maker.user_id) {
                            console.log("Logged in as "+req.user._id+" of type "+typeof(req.user._id));
                            console.log("Document owned by "+maker.user_id+" "+typeof(maker.user_id));
                            if (req.user._id.toString() == maker.user_id.toString()) {
                                callback(req, res);
                            } else {
                                res.send(Config.errorMsg.forbiddenError);
                            }
                        } else {
                            res.send(Config.errorMsg.noLinkedAccError);
                        }
                    } else {
                        res.send(Config.errorMsg.noMakerError);
                    }
                });
            } else {
                //no such wine
                console.error(err);
                res.status(404).send(Config.errorMsg.notFoundError);
            }
        });
    }
}

// Outlet for POST /wines/:wine_id/increment
exports.incrementOrders = function(req, res){
    console.log("increment orders");
    Wine.findByIdAndUpdate(
        {
            _id: req.params.wine_id
        },
        { $inc: {orders: 1}}

        , function (err, wine) {
            if (err) {
                console.error(err);
                res.status(500).send(Config.errorMsg.serverError);
            } else {
                res.status(200).json(wine._id);
            }
        });
}