var Config = require('./../Config/Config.js');
var Wine = require('./../models/wine');

// Outlet for GET /tags
// Returns all used tags in controllers
exports.getTags = function(req, res) {
    Wine.aggregate(
        { $project: { tags: "$tags"}},
        { $unwind: "$tags"},
        { $group: { _id : { name: "$tags.name", type: "$tags.type"}}},
        { $sort: { "_id.type": 1, "_id.name": 1}},
        function (err, tags) {
            if (err) {
                console.error(err);
                res.status(500).send(Config.errorMsg.serverError);
            } else
                res.json(tags);
        });
};

// Outlet for GET /tastes
// Returns all tags of controllers which match tags.type == "TASTE"
exports.getTastes = function(req, res) {
    Wine.aggregate(
        { $project: { tags: "$tags"}},
        { $unwind: "$tags"},
        { $group: { _id : { name: "$tags.name", type: "$tags.type"}}},
        { $match: {"_id.type": "TASTE"}},
        { $sort: {"_id.name": 1}},
        function (err, tags) {
            if (err) {
                console.error(err);
                res.status(500).send(Config.errorMsg.serverError);
            } else
                res.json(tags);
        });
};