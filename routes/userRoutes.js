module.exports = userRoutes;

function userRoutes(passport) {
    var userController = require('./../controllers/userController');
    var router = require('express').Router();

    router.post('/login', userController.login);
    router.post('/register', userController.register);
    router.post('/unregister', passport.authenticate('jwt', {session: false}), userController.unregister);

    return router;
}