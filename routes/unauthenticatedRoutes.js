module.exports = unauthenticatedRoutes;

function unauthenticatedRoutes() {

    var wineController = require('./../controllers/winesController');
    var tagsController = require('./../controllers/tagsController');
    var regionsController = require('./../controllers/regionsController');
    var recommendationsController = require('./../controllers/recommendationsController');
    var router = require('express').Router();

    //Set routes
    router.route('/wines')
        .get(wineController.get);

    router.route('/wines/:wine_id')
        .get(wineController.getById);

    router.route('/wines/:wine_id/increment')
        .put(wineController.incrementOrders);

    router.route('/wines/:wine_id/recommendations')
        .get(recommendationsController.get)
        .post(recommendationsController.post);

    router.route('/makers/:maker_id/wines')
        .get(wineController.getByMaker);

    router.route('/tags')
        .get(tagsController.getTags);

    router.route('/tastes')
        .get(tagsController.getTastes);
    
    router.route('/regions')
        .get(regionsController.getRegions);

    router.route('/search/tags/:tag_names')
        .get(wineController.getByTag);

    router.route('/search/filter')
        .get(wineController.getByFilterOptions);

    router.route('/search/:tag_names')
        .get(wineController.getByTag);


    return router;
}