module.exports = wineRoutes;

function wineRoutes(passport) {

    var makersController = require('./../controllers/makersController');
    var winesController = require('./../controllers/winesController');
    var recommendationsController = require('./../controllers/recommendationsController');
    var router = require('express').Router();

    //Authenticate user
    var mw = passport.authenticate('jwt', {session: false});
    mw.unless = require('express-unless');
    router.use(mw.unless({method: ['OPTIONS']}));

    // Set routes
    router.route('/makers')
        .post(makersController.post)
        .get(makersController.get);

    router.route('/makers/:maker_id')
        .get(makersController.getById)
        .put(makersController.put)
        .delete(makersController.delete);

    router.route('/own/wines/')
        .get(winesController.getOwn);

    router.route('/wines')
        .post(winesController.post);

    router.route('/wines/:wine_id')
        .put(winesController.put)
        .delete(winesController.delete);

    router.route('/wines/:wine_id/recommendations/:recommendation_id')
        .delete(recommendationsController.delete);

    return router;
}