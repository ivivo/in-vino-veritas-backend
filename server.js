var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var mongoose   = require('mongoose');
var Config     = require('./config/config');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 3000;

// ---------------
// DB Connection
// ---------------

mongoose.connect('mongodb://localhost/ivv-berta');
var db = mongoose.connection;
db.on('error', function () {
    throw new Error('unable to connect to database');
});

// ---------------
// ROUTES
// ---------------


// add headers to allow localhost cross origin access
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, authorization");

    // set allowed origin header
    var allowedOrigins = [
        'http://localhost',
        'http://localhost:63342'
    ];

    var origin = req.headers.origin;
    if(allowedOrigins.indexOf(origin) > -1){
        res.setHeader('Access-Control-Allow-Origin', origin);
    }

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Pass to next layer of middleware
    next();
});


//passport
var passport = require('passport');
var jwtConfig = require('./passport/jwtConfig');

app.use(passport.initialize());
jwtConfig(passport);

//General Error Handler
var serverErrorMsg = Config.errorMsg.serverError;
app.use(function (error, req, res, next) {
    if (error instanceof SyntaxError) {
        console.error("Error in request:\n" + error);
        res.status(500).send(serverErrorMsg);
    } else {
        next();
    }
});

// Register routes
var routes = require("./routes/routes");
var unauthenticatedRoutes = require('./routes/unauthenticatedRoutes');
var userRoutes = require("./routes/userRoutes");
app.use('/api',unauthenticatedRoutes());
app.use('/api',routes(passport));
app.use('/', userRoutes(passport));


// ---------------
// START
// ---------------

app.listen(port);
console.log('Listening on port ' + port);







