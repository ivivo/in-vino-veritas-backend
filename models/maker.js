var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var validate = require('mongoose-validator');


/*
Validators
 */


/*
Schema
 */
var makerSchema   = new Schema({
    name: {
        type: String,
        required: true
    },
    description: String,
    user_id: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    },
    region: {
        name: String,
        country: {
            type: String,
            required: true
        }
    },
    contacts: {
        tel: String,
        street: String,
        city: String,
        homepage: {type: String, validator: 'isURL'},
        mail: { type: String, validator: 'isEmail'}
    }
});

module.exports = mongoose.model('Maker', makerSchema);