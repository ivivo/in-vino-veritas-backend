var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var validate = require('mongoose-validator');

/*
Validators
 */
var starValidator =
    validate ({
        validator: function(stars){
            return !stars.isNull;
        },
        message: 'Stars must be filled in'
    });

var titleValidator =
    validate ({
        validator: function(title){
            return !title.isNull;
        },
        message: 'Title must be filled in'
    });

var authorValidator =
    validate ({
        validator: function(author){
            return !author.isNull;
        },
        message: 'Author must be filled in'
    });

var textValidator =
    validate ({
        validator: function(text){
            return !text.isNull;
        },
        message: 'Text must not be empty'
    });

/*
Schema
 */
var recommendationSchema = new Schema({
    stars: {
        type: Number,
        required: true,
        validate: starValidator
    },
    title: {
        type: String,
        required: true,
        validate: titleValidator
    },
    author: {
        type: String,
        required: true,
        validate: authorValidator
    },
    text: {
        type: String,
        required: true,
        validate: textValidator
    }
});

module.exports = mongoose.model('Recommendation', recommendationSchema);