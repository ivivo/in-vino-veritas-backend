var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var recommendationSchema = require('./recommendation').schema;
var validate = require('mongoose-validator');

/*
Validators
 */


var volumeValidator =

    validate ({
        validator: function(volume){
            return volume > 0.01 && volume < 5.01;
        },
        message: 'Das Flaschenvolumen muss zwischen 0.01 und 5.00 Liter liegen'
    });

var yearValidator = validate ({
    validator: function(year){
        return year > 1900 && year < 10000
    },
    message: 'Inkorrekter Jahrgang'
});


var priceValidator =
    validate ({
        validator: function(price){
            return price > 0.00
        },
        message: 'Negativer Preis'
    });

/*
 Schemas
 */

var tagSchema = new Schema({
    name: String,
    type: String
});

var wineSchema   = new Schema({
    name: {
        type: String,
        required: true
    },
    description: String,
    region: {
        name: String,
        country: String
    },
    maker: {
        type: Schema.ObjectId,
        ref: 'Maker',
        required: true
    },
    price: {
        type: Number,
        required: true,
        validate: priceValidator
    },
    year: {
        type: Number,
        required: true,
        validate: yearValidator
    },
    image: String,
    volume: {
        type: Number,
        required: true,
        validate: volumeValidator
    },
    tags: [tagSchema],
    recommendations: [recommendationSchema],
    orders: {
        type: Number,
        default: 0
    }
});

module.exports = mongoose.model('Wine', wineSchema);