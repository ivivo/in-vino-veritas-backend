var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var User = require('../models/user');
var Config = require('../config/config');


var options = {};
options.jwtFromRequest = ExtractJwt.fromAuthHeader();
options.secretOrKey = Config.auth.jwtSecret;

function verify(jwtPayload, done) {
    User.findOne({_id: jwtPayload.user._id}, function(err, user) {
        console.log("looking for user" + jwtPayload.user._id);
        if (err) {
            return done(err, false);
        }
        if (user) {
            console.log("found user: " + user);
            done(null, user);
        } else {
            done(null, false);
            // or you could create a new account
        }
    });
}


module.exports = function(passport){
    passport.use(new JwtStrategy(options, verify));
};
